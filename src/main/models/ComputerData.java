package main.models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Mateusz Dobrowolski on 2016-11-11.
 */
public class ComputerData {
    private static final String CONTEXT = "context";
    private static final Logger log = Logger.getLogger(ComputerData.class.getName());
    private static final String AUTHORIZED_KEY = "B[FXtz+ml.mk'2^";

    private String hashMd5;
    private String ip;
    private String computerName;
    private String nameOs;
    private String versionOs;
    private String architectureOS;
    private String accountName;

    public ComputerData() {
        ip = getPublicIpComputer();
        hashMd5 = generateMd5();
        computerName = computerName();
        nameOs = System.getProperty("os.name");
        versionOs = System.getProperty("os.version");
        architectureOS = System.getProperty("os.arch");
        accountName = System.getProperty("user.name");
    }

    public String generateMd5() {
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hash = md.digest(AUTHORIZED_KEY.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }
            digest = sb.toString();
        } catch (UnsupportedEncodingException ex) {
            log.log(Level.WARNING, CONTEXT, ex);
        } catch (NoSuchAlgorithmException ex) {
            log.log(Level.WARNING, CONTEXT, ex);
        }
        return digest;
    }

    public String getPublicIpComputer() {
        String ipAddress = "";
        URL whatismyip = null;
        try {
            whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
            ipAddress = in.readLine();
        } catch (MalformedURLException e) {
            log.log(Level.WARNING, CONTEXT, e);
        } catch (IOException e) {
            log.log(Level.WARNING, CONTEXT, e);
        }
        return ipAddress;
    }

    private String computerName() {
        Map<String, String> env = System.getenv();
        if (env.containsKey("COMPUTERNAME"))
            return env.get("COMPUTERNAME");
        else if (env.containsKey("HOSTNAME"))
            return env.get("HOSTNAME");
        else
            return "Unknown Computer";
    }

    public String getHashMd5() {
        return hashMd5;
    }

    public String getIp() {
        return ip;
    }

    public String getComputerName() {
        return computerName;
    }

    public String getNameOs() {
        return nameOs;
    }

    public String getVersionOs() {
        return versionOs;
    }

    public String getArchitectureOS() {
        return architectureOS;
    }

    public String getAccountName() {
        return accountName;
    }

    @Override
    public String toString() {

        return ip + " " + computerName + " " + nameOs + " " + versionOs + " " + architectureOS + " " + accountName;
    }
}
