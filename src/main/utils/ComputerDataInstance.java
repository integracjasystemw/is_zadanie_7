package main.utils;

import main.models.ComputerData;

/**
 * Created by Mateusz Dobrowolski on 2016-11-11.
 */
public class ComputerDataInstance {

    private static ComputerData computerData;

    private ComputerDataInstance() {
    }

    public static ComputerData getComputerData() {
        if (computerData == null) {
            computerData = new ComputerData();
        }
        return computerData;
    }
}
