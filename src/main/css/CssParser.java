package main.css;

import com.osbcp.cssparser.CSSParser;
import com.osbcp.cssparser.PropertyValue;
import com.osbcp.cssparser.Rule;
import com.osbcp.cssparser.Selector;
import javafx.scene.control.Alert;
import main.controller.Controller;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Mateusz Dobrowolski on 2016-12-22.
 */
public class CssParser {
    private static final String CONTEXT = "context";
    private static final Logger log = Logger.getLogger(Controller.class.getName());
    private static final String RESOURCES_DIR = "/resources/css/";

    private String fileName;
    private String absolutePath;
    private CustomizeCSS customizeCSS;
    private List<Rule> rules;

    public void setCustomizeCSS(CustomizeCSS customizeCSS) {
        this.customizeCSS = customizeCSS;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public void loadProperites() {

        String contents = null;
        try {
            contents = IOUtils.toString(this.getClass().getResourceAsStream(RESOURCES_DIR + fileName));

            rules = CSSParser.parse(contents);

            for (Rule rule : rules) {
                for (Selector selector : rule.getSelectors()) {
                    String name = selector.toString();
                    if (name.equals("caption[class=title]")) {
                        for (PropertyValue propertyValue : rule.getPropertyValues()) {
                            if (propertyValue.getProperty().equals("font-weight")) {
                                customizeCSS.setHeaderFontBold(propertyValue);
                            } else if (propertyValue.getProperty().equals("color")) {
                                customizeCSS.setHeaderColorPicker(propertyValue.getValue(), propertyValue);
                            } else if (propertyValue.getProperty().equals("font-size")) {
                                customizeCSS.setHeaderSize(propertyValue.getValue(), propertyValue);
                            }
                        }
                    } else if (name.equals("document headers")) {
                        for (PropertyValue propertyValue : rule.getPropertyValues()) {
                            if (propertyValue.getProperty().equals("font-weight")) {
                                customizeCSS.setFontBoldColumnName(propertyValue);
                            } else if (propertyValue.getProperty().equals("font-style")) {
                                customizeCSS.setFontItalicColumnName(propertyValue);
                            } else if (propertyValue.getProperty().equals("color")) {
                                customizeCSS.setColumnNameColorPicker(propertyValue.getValue(), propertyValue);
                            }
                        }
                    } else if (name.equals("document user")) {
                        for (PropertyValue propertyValue : rule.getPropertyValues()) {
                            if (propertyValue.getProperty().equals("font-weight")) {
                                customizeCSS.setFontBoldRecord(propertyValue);
                            } else if (propertyValue.getProperty().equals("color")) {
                                customizeCSS.setColorPickerNameRecord(propertyValue.getValue(), propertyValue);
                            }
                        }
                    } else if (name.equals("document headers > *")) {
                        for (PropertyValue propertyValue : rule.getPropertyValues()) {
                            if (propertyValue.getProperty().equals("background-color")) {
                                customizeCSS.setColorBackgroundColumnName(propertyValue.getValue(), propertyValue);
                            }
                        }
                    } else if (name.equals("document user > *")) {
                        for (PropertyValue propertyValue : rule.getPropertyValues()) {
                            if (propertyValue.getProperty().equals("border-color")) {
                                customizeCSS.setColorPickerBorder(propertyValue.getValue(), propertyValue);
                            } else if (propertyValue.getProperty().equals("border-width")) {
                                customizeCSS.setBorderSize(propertyValue.getValue(), propertyValue);
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            log.log(Level.WARNING, CONTEXT, e);
        } catch (Exception e) {
            log.log(Level.WARNING, CONTEXT, e);
        }
    }

    public void sout() {
        StringBuilder stringBuilder = new StringBuilder();
        for(Rule rule : rules) {
            stringBuilder.append(rule);
        }
        try {
            File file = new File(absolutePath.substring(0,absolutePath.lastIndexOf(File.separator))+"/style.css");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(stringBuilder.toString());
            fileWriter.close();
            customizeCSS.showDialog("Generator kaskadowego stylu CSS", "Wygenerowanego plik:" +file.getAbsolutePath(), Alert.AlertType.INFORMATION);
        } catch (IOException e) {
            log.log(Level.WARNING, CONTEXT, e);
            customizeCSS.showDialog("Generator kaskadowego stylu CSS", "Błąd podczas generowania pliku", Alert.AlertType.WARNING);
        }
    }

}
