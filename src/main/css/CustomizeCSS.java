package main.css;

import com.osbcp.cssparser.PropertyValue;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import main.components.dialogs.ErrorDialog;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Mateusz Dobrowolski on 2016-12-22.
 */
public class CustomizeCSS implements Initializable {
    private static final String COLOR_TRANSPARENTY = "#00000000";
    private static final String BOLD = "bold";
    private static final String ITALIC = "italic";

    private CssParser cssParser = new CssParser();
    @FXML
    private ChoiceBox<String> choiceBox;
    @FXML
    private ChoiceBox<String> headerSize;
    @FXML
    private ColorPicker headerColorPicker;
    @FXML
    private CheckBox headerFontBold;
    @FXML
    private CheckBox fontBoldColumnName;
    @FXML
    private CheckBox fontItalicColumnName;
    @FXML
    private ColorPicker colorBackgroundColumnName;
    @FXML
    private ColorPicker columnNameColorPicker;
    @FXML
    private ColorPicker colorPickerBorder;
    @FXML
    private ChoiceBox<String> borderSize;
    @FXML
    private ColorPicker colorPickerNameRecord;
    @FXML
    private CheckBox fontBoldRecord;

    public void setAbsolutePath(String path) {
        this.cssParser.setAbsolutePath(path);
    }

    public void showCustomizeWindow(String absolutePath) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("customizeCss.fxml"));
        Parent root = loader.load();
        ((CustomizeCSS) loader.getController()).setAbsolutePath(absolutePath);
        Stage stage = new Stage();
        stage.setTitle("Generator kaskadowego stylu CSS");
        stage.setScene(new Scene(root));
        stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cssParser.setCustomizeCSS(this);
        headerColorPicker.setValue(Color.web(COLOR_TRANSPARENTY));
        colorBackgroundColumnName.setValue(Color.web(COLOR_TRANSPARENTY));
        columnNameColorPicker.setValue(Color.web(COLOR_TRANSPARENTY));
        colorPickerBorder.setValue(Color.web(COLOR_TRANSPARENTY));


        choiceBox.getItems().setAll(FXCollections.observableArrayList("Style 1", "Style 2", "Style 3"));
        choiceBox.getSelectionModel().select(0);
        loadProperites(0);
        choiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                loadProperites(newValue.intValue());
            }
        });
        borderSize.getItems().setAll(FXCollections.observableArrayList("0px", "0.5px", "1px", "1.5px", "2px", "2.5px", "3px"));
        headerSize.getItems().setAll(FXCollections.observableArrayList("0.5em", "1em", "1.5em", "2em", "2.5em", "3em", "3.5em", "4em"));
    }

    private void loadProperites(int index) {
        switch (index) {
            case 0:
                cssParser.setFileName("style1.css");
                break;
            case 1:
                cssParser.setFileName("style2.css");
                break;
            case 2:
                cssParser.setFileName("style3.css");
                break;
            default:
                cssParser.setFileName("style1.css");
        }
        cssParser.loadProperites();
    }

    public void setHeaderSize(String size, PropertyValue propertyValue) {
        headerSize.getSelectionModel().select(size);
        headerSize.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(oldValue.intValue() != -1) {
                    propertyValue.setValue(headerSize.getItems().get(newValue.intValue()));
                }
            }
        });
    }

    public void setHeaderColorPicker(String colorHex, PropertyValue propertyValue) {
        headerColorPicker.setValue(Color.web(colorHex));
        headerColorPicker.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
                setColor(propertyValue, newValue);
            }
        });
    }

    public void setHeaderFontBold(PropertyValue propertyValue) {
        if(propertyValue.getValue().equals(BOLD)) {
            headerFontBold.setSelected(true);
        }
        headerFontBold.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                setFontWeight(propertyValue, newValue);
            }
        });
    }

    public void setFontBoldColumnName(PropertyValue propertyValue) {
        if(propertyValue.getValue().equals(BOLD)) {
            fontBoldColumnName.setSelected(true);
        }
        fontBoldColumnName.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                setFontWeight(propertyValue, newValue);
            }
        });
    }

    public void setFontItalicColumnName(PropertyValue propertyValue) {
        if(propertyValue.getValue().equals(ITALIC)) {
            fontItalicColumnName.setSelected(true);
        }
        fontItalicColumnName.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    propertyValue.setValue(ITALIC);
                } else {
                    propertyValue.setValue("normal");
                }
            }
        });
    }

    public void setColorBackgroundColumnName(String colorHex, PropertyValue propertyValue) {
        colorBackgroundColumnName.setValue(Color.web(colorHex));
        colorBackgroundColumnName.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
                setColor(propertyValue, newValue);
            }
        });
    }

    public void setColumnNameColorPicker(String colorHex, PropertyValue propertyValue) {
        columnNameColorPicker.setValue(Color.web(colorHex));
        columnNameColorPicker.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
                setColor(propertyValue, newValue);
            }
        });
    }

    public void setColorPickerBorder(String colorHex, PropertyValue propertyValue) {
        colorPickerBorder.setValue(Color.web(colorHex));
        colorPickerBorder.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
                setColor(propertyValue, newValue);
            }
        });
    }

    public void setBorderSize(String size, PropertyValue propertyValue) {
        borderSize.getSelectionModel().select(size);
        borderSize.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                propertyValue.setValue(borderSize.getItems().get(newValue.intValue()));
            }
        });
    }

    public void setColorPickerNameRecord(String colorHex, PropertyValue propertyValue) {
        colorPickerNameRecord.setValue(Color.web(colorHex));
        colorPickerNameRecord.valueProperty().addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
                setColor(propertyValue, newValue);
            }
        });
    }

    public void setFontBoldRecord(PropertyValue propertyValue) {
        if(propertyValue.getValue().equals(BOLD)) {
            fontBoldRecord.setSelected(true);
        }
        fontBoldRecord.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                setFontWeight(propertyValue, newValue);
            }
        });
    }

    private void setFontWeight(PropertyValue propertyValue, boolean isBold){
        if (isBold) {
            propertyValue.setValue("bold");
        } else {
            propertyValue.setValue("normal");
        }
    }

    private void setColor(PropertyValue propertyValue, Color color) {
        String hex = String.valueOf(color);
        int indexOf = hex.indexOf('f');
        propertyValue.setValue(String.format("#%s", hex.substring(2, indexOf != -1 ?  hex.length()-2 : hex.length())));
    }

    @FXML
    public void generateCssFile() {
        cssParser.sout();
    }

    public void showDialog(String title, String text, Alert.AlertType alertType) {
        Platform.runLater(() -> new ErrorDialog(title, text, alertType).showAndWait());
    }
}
