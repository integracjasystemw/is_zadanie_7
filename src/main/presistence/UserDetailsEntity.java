package main.presistence;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Mateusz Dobrowolski on 2016-11-01.
 */

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "findAccountsOlderThan",
                procedureName = "findAccountsOlderThan",
                resultClasses = {UserDetailsEntity.class},
                parameters = {
                        @StoredProcedureParameter(name = "data", type = Date.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userMd5", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "publicIp", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "computerName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "nameOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "versionOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "archOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "accountName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "successOut", type = Integer.class, mode = ParameterMode.OUT)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "findUser",
                procedureName = "findUser",
                resultClasses = {UserDetailsEntity.class},
                parameters = {
                        @StoredProcedureParameter(name = "userName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userMd5", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "publicIp", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "computerName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "nameOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "versionOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "archOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "accountName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "successOut", type = Integer.class, mode = ParameterMode.OUT)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "findUserActiveFromGivenCountOfMonths",
                procedureName = "findUserActiveFromGivenCountOfMonths",
                resultClasses = {UserDetailsEntity.class},
                parameters = {
                        @StoredProcedureParameter(name = "numberMonths", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userMd5", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "publicIp", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "computerName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "nameOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "versionOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "archOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "accountName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "successOut", type = Integer.class, mode = ParameterMode.OUT)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "findPersonByGender",
                procedureName = "findPersonByGender",
                resultClasses = {UserDetailsEntity.class},
                parameters = {
                        @StoredProcedureParameter(name = "gender", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userMd5", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "publicIp", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "computerName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "nameOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "versionOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "archOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "accountName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "successOut", type = Integer.class, mode = ParameterMode.OUT)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "findUserByMobilePhone",
                procedureName = "findUserByMobilePhone",
                resultClasses = {UserDetailsEntity.class},
                parameters = {
                        @StoredProcedureParameter(name = "numberMobilePhone", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userMd5", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "publicIp", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "computerName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "nameOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "versionOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "archOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "accountName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "successOut", type = Integer.class, mode = ParameterMode.OUT)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "showUsersUsedGivenMobileOperator",
                procedureName = "showUsersUsedGivenMobileOperator",
                resultClasses = {UserDetailsEntity.class},
                parameters = {
                        @StoredProcedureParameter(name = "mobileOperator", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userMd5", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "publicIp", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "computerName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "nameOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "versionOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "archOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "accountName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "successOut", type = Integer.class, mode = ParameterMode.OUT)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "findUsersByServiceOfWebMail",
                procedureName = "findUsersByServiceOfWebMail",
                resultClasses = {UserDetailsEntity.class},
                parameters = {
                        @StoredProcedureParameter(name = "webMail", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userMd5", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "publicIp", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "computerName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "nameOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "versionOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "archOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "accountName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "successOut", type = Integer.class, mode = ParameterMode.OUT)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "findUsersByStatus",
                procedureName = "findUsersByStatus",
                resultClasses = {UserDetailsEntity.class},
                parameters = {
                        @StoredProcedureParameter(name = "status", type = Integer.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userMd5", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "publicIp", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "computerName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "nameOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "versionOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "archOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "accountName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "successOut", type = Integer.class, mode = ParameterMode.OUT)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "findUsersFromGivenAddress",
                procedureName = "findUsersFromGivenAddress",
                resultClasses = {UserDetailsEntity.class},
                parameters = {
                        @StoredProcedureParameter(name = "city", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "street", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userMd5", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "publicIp", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "computerName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "nameOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "versionOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "archOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "accountName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "successOut", type = Integer.class, mode = ParameterMode.OUT)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "findUserLogginIngivenPeriodOfTime",
                procedureName = "findUserLogginIngivenPeriodOfTime",
                resultClasses = {UserDetailsEntity.class},
                parameters = {
                        @StoredProcedureParameter(name = "fromDate", type = Date.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "toDate", type = Date.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "userMd5", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "publicIp", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "computerName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "nameOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "versionOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "archOs", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "accountName", type = String.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(name = "successOut", type = Integer.class, mode = ParameterMode.OUT)
                }
        )}
)
@Entity
@Table(name = "user_details", schema = "isdb", catalog = "")
public class UserDetailsEntity {
    private int userId;
    private String username;
    private String firstName;
    private String lastName;
    private String gender;
    private String city;
    private String street;
    private Integer number;
    private String email;
    private String mobileNumber;
    private String phoneNumber;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "last_name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "mobileNumber")
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Basic
    @Column(name = "number")
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }


    @Basic
    @Column(name = "phoneNumber")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserDetailsEntity that = (UserDetailsEntity) o;

        if (userId != that.userId) {
            return false;
        }
        if (city != null ? !city.equals(that.city) : that.city != null) {
            return false;
        }
        if (email != null ? !email.equals(that.email) : that.email != null) {
            return false;
        }
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) {
            return false;
        }
        if (gender != null ? !gender.equals(that.gender) : that.gender != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) {
            return false;
        }
        if (mobileNumber != null ? !mobileNumber.equals(that.mobileNumber) : that.mobileNumber != null) {
            return false;
        }
        if (number != null ? !number.equals(that.number) : that.number != null) {
            return false;
        }
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) {
            return false;
        }
        if (street != null ? !street.equals(that.street) : that.street != null) {
            return false;
        }
        if (username != null ? !username.equals(that.username) : that.username != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (mobileNumber != null ? mobileNumber.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        return result;
    }
}
