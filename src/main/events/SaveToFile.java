package main.events;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.google.gson.Gson;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;
import main.Main;
import main.css.CustomizeCSS;
import main.lists.Results;
import main.presistence.UserDetailsEntity;
import main.xml.XmlObject;
import net.sourceforge.yamlbeans.YamlException;
import net.sourceforge.yamlbeans.YamlWriter;
import ogdl.Graph;
import ogdl.IGraph;
import ogdl.OgdlEmitter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Mateusz Dobrowolski on 2016-12-04.
 */
public class SaveToFile implements EventHandler {
    private static final String CONTEXT = "context";
    private static final Logger log = Logger.getLogger(SaveToFile.class.getName());
    ObservableList<UserDetailsEntity> data;
    List<UserDetailsEntity> userDetailsEntityList;

    @Override
    public void handle(Event event) {
        userDetailsEntityList = Main.controller.getResultUserDetailsEntityList();
        data = Main.controller.getData();
        File file = getFileByFileChooser();
        try {
            if (file != null) {
                if (file.getAbsolutePath().endsWith(".yml")) {
                    saveToYmlFile(file);
                } else if (file.getAbsolutePath().endsWith(".xml")) {
                    saveToXmlFile(file);
                } else if (file.getAbsolutePath().endsWith(".json")) {
                    saveToJsonFile(file);
                } else if (file.getAbsolutePath().endsWith(".ogdl")) {
                    saveToOgdlFile(file);
                }
            }
        } catch (Exception e) {
            log.log(Level.WARNING, CONTEXT, e);
        }
    }

    private File getFileByFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("YAML files (*.yml)", "*.yml"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("OGDL files (*.ogdl)", "*.ogdl"));
        return fileChooser.showSaveDialog(null);
    }

    private void saveToYmlFile(File file) throws IOException, YamlException {
        YamlWriter writer = new YamlWriter(new FileWriter(file));
        writer.getConfig().setClassTag("results", Results.class);
        writer.getConfig().setPropertyElementType(Results.class, "users", UserDetailsEntity.class);
        writer.write(new Results(userDetailsEntityList));
        writer.close();
    }

    private void saveToXmlFile(File file) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        JaxbAnnotationModule jaxbAnnotationModule = new JaxbAnnotationModule();
        xmlMapper.registerModule(jaxbAnnotationModule);
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Title of table input dialog");
        dialog.setHeaderText("Information");
        dialog.setContentText("Please enter your title of table:");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent() && !dialog.getEditor().getText().trim().isEmpty()) {
            String xmlString = xmlMapper.writeValueAsString(new XmlObject(userDetailsEntityList, result.get()));
            StringBuilder xmlStringBuilder = new StringBuilder(xmlString);

            xmlStringBuilder.insert(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\n<?xml-stylesheet type=\"text/css\" href=\"style.css\"?>");
            xmlString = xmlStringBuilder.toString();

            Document document = Jsoup.parse(xmlString, "", Parser.xmlParser());
            document.select("caption").addClass("title");

            Writer fileWriter = new OutputStreamWriter(new FileOutputStream(file.getAbsolutePath()), StandardCharsets.UTF_8);
            fileWriter.write(document.toString());
            fileWriter.close();

            CustomizeCSS customizeCSS = new CustomizeCSS();
            customizeCSS.showCustomizeWindow(file.getAbsolutePath());
        } else {
            Main.controller.showDialog("Brak tytułu", "Nie podałeś tytułu tabeli", Alert.AlertType.ERROR);
        }

    }

    private void saveToJsonFile(File file) throws IOException {
        Writer writer = new FileWriter(file.getAbsolutePath());
        Gson gson = new Gson();
        gson.toJson(new Results(userDetailsEntityList), writer);
        writer.close();
    }

    private void saveToOgdlFile(File file) throws Exception {
        IGraph noodeGraph = new Graph();
        for (UserDetailsEntity userDetailsEntity : userDetailsEntityList) {
            IGraph userGraph = new Graph();
            userGraph.add("user_id", String.valueOf(userDetailsEntity.getUserId()));
            userGraph.add("username", userDetailsEntity.getUsername());
            userGraph.add("firstname", userDetailsEntity.getFirstName());
            userGraph.add("lastname", userDetailsEntity.getLastName());
            userGraph.add("city", userDetailsEntity.getCity());
            userGraph.add("street", userDetailsEntity.getStreet());
            userGraph.add("email", userDetailsEntity.getEmail());
            userGraph.add("number", String.valueOf(userDetailsEntity.getNumber()));
            userGraph.add("phone_number", String.valueOf(userDetailsEntity.getPhoneNumber()));
            userGraph.add("mobile_number", userDetailsEntity.getMobileNumber());
            userGraph.add("gender", userDetailsEntity.getGender());
            noodeGraph.add("users", OgdlEmitter.toString(userGraph));
        }
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(OgdlEmitter.toString(noodeGraph));
        fileWriter.close();
    }
}
