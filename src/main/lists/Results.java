package main.lists;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.google.gson.annotations.SerializedName;
import main.presistence.UserDetailsEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz Dobrowolski on 2016-11-03.
 */
@JacksonXmlRootElement(localName = "results")
public class Results implements Serializable {
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "user")
    @SerializedName("users")
    public List<UserDetailsEntity> users;

    public Results(){}

    public Results(UserDetailsEntity userDetailsEntity) {
        users = new ArrayList<>();
        users.add(userDetailsEntity);
    }

    public Results(List<UserDetailsEntity> userDetailsEntityList) {
        this.users = userDetailsEntityList;
    }
}
