package main.components.dialogs;

import javafx.scene.control.Alert;

/**
 * Created by Mateusz Dobrowolski on 2016-11-06.
 */
public class ErrorDialog extends Alert {

    public ErrorDialog(String title, String contextText, AlertType alertType) {
        super(alertType);
        setTitle(title);
        setHeaderText(contextText);
    }
}
