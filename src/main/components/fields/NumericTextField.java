package main.components.fields;

import javafx.scene.control.TextField;

/**
 * Created by Mateusz Dobrowolski on 2016-11-05.
 */
public class NumericTextField extends TextField {
    @Override
    public void replaceText(int start, int end, String text) {
        if (text.matches("[0-9]*")) {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String text) {
        if (text.matches("[0-9]*")) {
            super.replaceSelection(text);
        }
    }
}
