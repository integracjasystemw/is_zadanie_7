package main.components.menuitem;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-11-04.
 */
public class MenuItemFindPersonsByGender extends MenuItem {
    public MenuItemFindPersonsByGender create() {
        setText("Find persons by gender");
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Main.controller.setLabelFieldText("Choose gender: ");
                Main.controller.setOneFieldComboSearching(FXCollections.observableArrayList("Male", "Female"));
                Main.controller.indexMenuItem = 4;
            }
        });
        return this;
    }
}
