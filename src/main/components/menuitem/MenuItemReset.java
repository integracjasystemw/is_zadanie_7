package main.components.menuitem;


import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-10-26.
 */
public class MenuItemReset extends MenuItem {
    public MenuItemReset create() {
        setText("Reset");
        setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Main.controller.clearData();
            }
        });
        return this;
    }

}
