package main.components.menuitem;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-11-04.
 */
public class MenuItemFindAccountsOlderFromGivenOfData extends MenuItem {
    public MenuItemFindAccountsOlderFromGivenOfData create() {
        setText("Find accounts older from given of data");
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Main.controller.setLabelFieldText("Find accounts older from entered data");
                Main.controller.setOneFieldDataSearching();
                Main.controller.indexMenuItem = 2;
            }
        });
        return this;
    }
}
