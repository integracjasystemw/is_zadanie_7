package main.components.menuitem;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-11-04.
 */
public class MenuItemFindUsersByMobilePhone extends MenuItem {
    public MenuItemFindUsersByMobilePhone create() {
        setText("Find user by mobile phone");
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Main.controller.setLabelFieldText("Mobile phone number:");
                Main.controller.setOneFieldTextSearching();
                Main.controller.indexMenuItem = 6;
            }
        });
        return this;
    }
}
