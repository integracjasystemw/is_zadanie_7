package main.components.menuitem;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-11-04.
 */
public class MenuItemShowUsersUsedGivenMobileOperator extends MenuItem {
    public MenuItemShowUsersUsedGivenMobileOperator create() {
        setText("Show users used given mobile operator");
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.controller.setLabelFieldText("Choose mobile operator (HANDLED general prefix!): ");
                Main.controller.setOneFieldComboSearching(FXCollections.observableArrayList("Orange","Play","Heyah","Plus","T-Mobile"));
                Main.controller.indexMenuItem = 9;
            }
        });
        return this;
    }
}
