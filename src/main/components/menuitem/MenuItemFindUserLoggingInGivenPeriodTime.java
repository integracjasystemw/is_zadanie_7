package main.components.menuitem;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import main.Main;

/**
 * Created by Mateusz Dobrowolski on 2016-11-02.
 */
public class MenuItemFindUserLoggingInGivenPeriodTime extends MenuItem {
   public MenuItemFindUserLoggingInGivenPeriodTime create() {
        setText("Find user loggin in a given period of time");
        setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                Main.controller.setLabelFieldText("Period time:");
                Main.controller.setTwoFieldDataSearching();
                Main.controller.indexMenuItem = 7;
            }
        });
        return this;
    }
}


