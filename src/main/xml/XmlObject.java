package main.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import main.presistence.UserDetailsEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz Dobrowolski on 2016-11-03.
 */

@JacksonXmlRootElement(localName = "document")
public class XmlObject implements Serializable {
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "caption")
    String caption;
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "headers")
    Headers headers = new Headers();
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "user")
    public List<UserDetailsEntity> users;

    public XmlObject(UserDetailsEntity userDetailsEntity) {
        users = new ArrayList<>();
        users.add(userDetailsEntity);
    }

    public XmlObject(List<UserDetailsEntity> userDetailsEntityList, String caption) {
        this.users = userDetailsEntityList;
        this.caption = caption;
    }
}
