package main.xml;

import java.io.Serializable;

/**
 * Created by Mateusz Dobrowolski on 2016-12-22.
 */

public class Headers implements Serializable {
    private String userId = "User ID";
    private String username = "Username";
    private String firstName = "First name";
    private String lastName = "Last name";
    private String gender = "Gender";
    private String city = "City";
    private String street = "Street";
    private String number = "Number";
    private String email = "Email";
    private String mobileNumber = "Mobile Number";
    private String phoneNumber = "Phone Number";

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
        return gender;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getNumber() {
        return number;
    }

    public String getEmail() {
        return email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
