package main.builder;

import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 * Created by Mateusz Dobrowolski on 2016-11-11.
 */
public class StageBuilder {

    private StageBuilder() {
    }

    public static Parent buildRoot(Parent vBox) {
        BorderPane borderPane = new BorderPane();
        borderPane.setTop(buildMenuBar());
        borderPane.setCenter(vBox);
        borderPane.setBottom(buildBottom());
        return borderPane;
    }

    public static MenuBar buildMenuBar(){
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(new MenuSettingsBuilder().create(), new MenuSearchMethodBuilder().create());
        return menuBar;
    }

    public static HBox buildBottom(){
        HBox bottom = new HBox();
        bottom.setStyle("-fx-background-color: rgba(0, 0, 0, 0.07);");
        bottom.setStyle("-fx-border-color: rgba(0, 0, 0, 0.27);");
        bottom.getChildren().add(new Label());
        return bottom;
    }
}
