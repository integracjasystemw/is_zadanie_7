package main.builder;

import javafx.scene.control.Menu;
import main.components.menuitem.MenuItemExit;
import main.components.menuitem.MenuItemReset;

/**
 * Created by Mateusz Dobrowolski on 2016-10-26.
 */
public class MenuSettingsBuilder extends Menu {

    public Menu create(){
        setText("Settings");
        getItems().add(new MenuItemReset().create());
        getItems().add(new MenuItemExit().create());
        return this;
    }

}
