package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.builder.StageBuilder;
import main.controller.Controller;
import main.css.CustomizeCSS;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

public class Main extends Application {
    public static Controller controller;
    public static File file;
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Aplikacja do wyświetlania danych z bazy danych");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("gui.fxml"));
        Parent vBox = loader.load();
        controller = loader.getController();
        primaryStage.setScene(new Scene(StageBuilder.buildRoot(vBox)));
        primaryStage.setResizable(false);
        primaryStage.show();
        controller.setRoot((BorderPane) primaryStage.getScene().getRoot());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
